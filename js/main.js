//* INFO/CS 1300
//* Fall 2016
//* Isabelle De Brabanter


var pic_array = ["pate.jpg", "steakfrites.jpg", "pizza.jpg", "salade.jpg"];
// Media Credit for all 4 pictures : Isabelle De Brabanter 
var index = 0;

function next_image() {
// This function codes for the next image in the slideshow. When you click the next button, the next image in the sequence should show up. 
    "use strict";
    var init_pic = document.getElementById('pate');
    if (index < pic_array.length - 1) {
        index += 1;
        init_pic.src = 'images/' + pic_array[index];
    }
    
    else  {
        index = 0;
        init_pic.src = 'images/' + pic_array[index];
    }  
}

function previous_image() {
// This function codes for the previous image in the slideshow. When you click the previous button, the previous image in the sequence should show up. 
    "use strict";
    var init_pic = document.getElementById('pate');
    if (index == 0) {
        index = pic_array.length - 1;
        init_pic.src = 'images/' + pic_array[index];
    }
    else {
        index -= 1;
        init_pic.src = 'images/' + pic_array[index];
    }
}

window.onload = function date_generator() {
// This function codes for the date to be present in the footer of the page in the format Day Month Year. 
    "use strict";
    // variables
     var month_list = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        date = new Date(),
        month = month_list[date.getMonth()],
        day = date.getDate(),
        year = date.getFullYear(),
        foot = document.createTextNode(day+" "+month+" "+year),
        newspan = document.createElement('span'),
        position = document.getElementsByTagName('footer')[0];
        newspan.appendChild(foot);
        position.appendChild(newspan);

}

